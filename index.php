<!doctype html>
<html lang="ru">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <title>Hello!</title>
</head>
<body>
<h1 align="center" title="Phone might be impacting your performance">Пример заголовка 1 уровня</h1>
<h2 align="center">Пример заголовка 2 уровня</h2>
<p><strong>Lorem Ipsum</strong> - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала
    XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.
<a href="mailto:violamorena@gmail.com">Напиши мне</a>

    <br/>Lorem Ipsum не
    только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset
    с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.
    <a href="#green">Go to the bottom</a>
</p>
<img src="Pictures/gregorian.jpg" alt="Картинка" width="150" />
<p><strong><em>Lorem Ipsum</em></strong> - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице
    с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum
    не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letras
    et с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.
    <a href="http://21vek.by" target="_blank">Клёвый магазин тут</a> <a href="page.php">Ссылка</a>

</p>
<img src="Pictures/Royal.jpg" alt="Картинка" width="150" align="right" />
<ol type="A">
    <li>Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинс кий набор слов, но это не совсем так. Его корни уходят в один фрагмент классической латыни 45 года
        н.э., то есть более двух тысячелетий назад.</li>
    <li>Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов, но это не совсем так. Его корни уходят в один фрагмент классической латыни 45 года
        н.э., то есть более двух тысячелетий назад.</li>
    <li>Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов, но это не совсем так. Его корни уходят в один фрагмент классической латыни 45 года
        н.э., то есть более двух тысячелетий назад.</li>
</ol>
<ul type="square">
    <li>Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов, но это не совсем так. Его корни уходят в один фрагмент классической латыни 45 года
        н.э., то есть более двух тысячелетий назад.</li>
    <li>Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов, но это не совсем так. Его корни уходят в один фрагмент классической латыни 45 года
        н.э., то есть более двух тысячелетий назад.</li>
    <li>Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов, но это не совсем так. Его корни уходят в один фрагмент классической латыни 45 года
        н.э., то есть более двух тысячелетий назад.</li>
</ul>
<ol>
    <li>Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов.
        <ul type="circle">
            <li>Его корни уходят в один фрагмент классической латыни</li>
            <li>Его корни уходят в один фрагмент классической латыни</li>
        </ul>
    </li>
    <li>Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов.
        <ul type="circle">
            <li>Его популяризации в новое время послужили публикация листов Letras</li>
            <li>Его популяризации в новое время послужили публикация листов Letras</li>
            <li>Его популяризации в новое время послужили публикация листов Letras</li>
        </ul>
    </li>
    <li>Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов.
        <ul type="square">
            <li>Есть много вариантов Lorem Ipsum</li>
            <li>Есть много вариантов Lorem Ipsum</li>
        </ul>
    </li>
</ol>
<p><a href="Files/file.jpg">Download me!</a></p>
<a id="green" href="Files/file.jpg"><img src="Pictures/button.png" alt="Скачать" width="150"/></a>

<p>
<table border="1" width="650" cellpadding="10">
   <tr>
       <th>Выберите комплект</th>
       <th>Стандарт</th>
       <th>Голд</th>
       <th>Премиум</th>
   </tr>
   <tr align="center">
        <td align="left">Основной видеокурс</td>
        <td><img src="Pictures/yes.png" alt="Да" width="30"/></td>
        <td><img src="Pictures/yes.png" alt="Да" width="30"/></td>
        <td><img src="Pictures/yes.png" alt="Да" width="30"/></td>
    </tr>
    <tr align="center">
        <td align="left">Продвинутый видеокурс</td>
        <td><img src="Pictures/no.png" alt="Нет" width="30"/></td>
        <td><img src="Pictures/yes.png" alt="Да" width="30"/></td>
        <td><img src="Pictures/yes.png" alt="Да" width="30"/></td>
    </tr>
    <tr align="center">
        <td align="left">wwerwer</td>
        <td><img src="Pictures/no.png" alt="Нет" width="30"/></td>
        <td><img src="Pictures/no.png" alt="Нет" width="30"/></td>
        <td><img src="Pictures/yes.png" alt="Да" width="30"/></td>
    </tr>
    <tr align="center">
        <td rowspan="2"></td>
        <td><strong>15000</strong> руб.</td>
        <td><strong>2500</strong> руб.</td>
        <td><strong>35000</strong> руб.</td>
    </tr>
    <tr align="center">
        <td><a href="#">Заказать</a></td>
        <td><a href="#">Заказать</a></td>
        <td><a href="#">Заказать</a></td>
    </tr>
</table>
</p>

</body>
</html>